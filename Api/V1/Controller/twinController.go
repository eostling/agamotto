package Controller

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"novetta/Agamotto/Api/V1/Controller/responses"
	"novetta/Agamotto/Api/V1/Model"
	"strconv"
)

// TODO: Next will be to refactor to use aache

func (server *Server) CreateTwin(w http.ResponseWriter, r *http.Request){

	responseBody, err := ioutil.ReadAll(r.Body)

	if err != nil {
		responses.FormatError(w, http.StatusUnprocessableEntity, err)
	}

	twin := Model.Twin{}

	// TODO: Debug this
	err = json.Unmarshal(responseBody, &twin)

	if err != nil {
		responses.FormatError(w, http.StatusUnprocessableEntity, err)
		return
	}

	twinCreated, err := twin.SaveTwin(server.DB)

	if err != nil {
		responses.FormatError(w, http.StatusInternalServerError, err)
		return
	}

	responses.FormatJSON(w, http.StatusCreated, twinCreated)
}

// TODO: Next will be to refactor to use cache

func (server *Server) UpdateTwin(w http.ResponseWriter, r *http.Request){
	// creates a map of routed variables to access
	vars := mux.Vars(r)

	// Package strconv implements conversions to and from string representations of basic data types.
	twinID, err := strconv.ParseUint(vars["ThingId"], 10, 64)

	if err != nil {
		responses.FormatError(w, http.StatusBadRequest, err)
		return
	}

	twin := Model.Twin{}
	err = server.DB.Debug().Model(Model.Twin{}).Where("ThingId = ?", twinID).Take(&twin).Error

	if err != nil {
		responses.FormatError(w, http.StatusNotFound, errors.New("Twin Record not found"))
		return
	}

	responseBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.FormatError(w, http.StatusUnprocessableEntity, err)
		return
	}

	// This is the new model that will be persisted
	twinRecord := Model.Twin{}
	err = json.Unmarshal(responseBody, &twinRecord)

	if err != nil {
		responses.FormatError(w, http.StatusUnprocessableEntity, err)
		return
	}

	// set the twin record to be updated
	twinRecord.ThingId = twin.ThingId

	// update the twin record
	updatedTwin, err := twinRecord.UpdateTwin(server.DB)

	if err != nil {
		responses.FormatError(w, http.StatusInternalServerError, err)
		return
	}

	responses.FormatJSON(w, http.StatusOK, updatedTwin)
}

func (server *Server) GetAllTwins(w http.ResponseWriter, r *http.Request) {
	twin := Model.Twin{}

	twins, err := twin.GetAllTwins(server.DB)

	if err != nil{
		responses.FormatError(w, http.StatusInternalServerError, err)
		return
	}
	responses.FormatJSON(w, http.StatusOK, twins)

}





