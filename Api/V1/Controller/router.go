package Controller

import "novetta/Agamotto/Api/V1/Middleware"

func (server *Server) routes(){
	// GET
	server.Router.HandleFunc("/", Middleware.JsonMiddleWare(server.Default)).Methods("GET")
	server.Router.HandleFunc("/twins", Middleware.JsonMiddleWare(server.GetAllTwins)).Methods("GET")
	// POST/PUT
	server.Router.HandleFunc("/twins", Middleware.JsonMiddleWare(server.CreateTwin)).Methods("POST")
	server.Router.HandleFunc("/twins/{id}",Middleware.JsonMiddleWare(server.UpdateTwin)).Methods("PUT")
}