package responses

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func FormatJSON(w http.ResponseWriter, statusCode int, data interface{}) {
	w.WriteHeader(statusCode)
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		fmt.Fprintf(w, "%s", err.Error())
	}
}

func FormatError(w http.ResponseWriter, statusCode int, err error) {
	if err != nil {
		FormatJSON(w, statusCode, struct {
			Error string `json:"error"`
		}{
			Error: err.Error(),
		})
		return
	}
	FormatJSON(w, http.StatusBadRequest, nil)
}