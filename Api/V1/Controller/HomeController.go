package Controller

import (
	"net/http"
	"novetta/Agamotto/Api/V1/Controller/responses"
)

func (server *Server) Default(w http.ResponseWriter, r *http.Request) {
	responses.FormatJSON(w, http.StatusOK, "Twin Service is up and running.")
}