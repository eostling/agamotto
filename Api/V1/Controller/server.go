package Controller

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"net/http"
	"novetta/Agamotto/Api/V1/Model"
)

type Server struct {
	DB     *gorm.DB
	Router *mux.Router
}

func (server *Server) Initialize(Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string) {
	var err error

	if Dbdriver == "postgres" {
		DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DbHost, DbPort, DbUser, DbName, DbPassword)
		server.DB, err = gorm.Open(Dbdriver, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database", Dbdriver)
			log.Fatal("Setting up the database has failed", err)
		} else {
			fmt.Printf("We are connected to the %s database", Dbdriver) // this will get swapped out
		}
	}
	// Migrate
	server.DB.Debug().AutoMigrate(&Model.Twin{})
	// Setup routes
	server.Router = mux.NewRouter()
	server.routes()
}

func (server *Server) Serve(port string) {
	fmt.Println("Listening to port 8000")
	log.Fatal(http.ListenAndServe(port, server.Router))
}