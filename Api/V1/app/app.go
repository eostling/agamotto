package app

import (
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"novetta/Agamotto/Api/V1/Controller"
	"novetta/Agamotto/Api/V1/seed"
	"os"
)

//var cache CacheStore
var server = Controller.Server{}

func Run(){
	var err error
	err = godotenv.Load()
	if err != nil {
		log.Fatalf("Error getting env, not comming through %v", err)
	} else {
		fmt.Println("Standing up database and what not ")
	}
	// initialize it
	server.Initialize(os.Getenv("DB_DRIVER"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_PORT"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME"))
	// dummy data; Remove before deploying
	seed.Load(server.DB)
	server.Serve(":8000")
}

func validate(e error){
	if e != nil {
		log.Println(e)
		os.Exit(1)
	}
}
