package Model

import "container/list"

type EvictionCallback func(key interface{}, value interface{})

type LRU struct{
	size int
	evictionList *list.List
	items map[interface{}]*list.Element
	onEvition EvictionCallback
}

type entry struct{
	key interface{}
	value interface{}
}

/**
LRU API should implement the following functions

Contructor()
Purge()/Invalidate()
Add
Get
Remove
GetOldest
RemoveOldest
contains
length
removeElement(elem)

 */