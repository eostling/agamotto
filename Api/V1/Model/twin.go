package Model

import (
	"github.com/jinzhu/gorm"
	"time"
)

type Twin struct {
	gorm.Model
	ThingId      uint32    `gorm:"primary_key;auto_increment" json:"thingId"`
	NamedTwin    string    `gorm:"size:100;not null;unique" json:"namedTwin"`
	LastModified time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"lastModified"`
	Value        string    `gorm:"size:200;not null;" json:"value"`
}

func (twin *Twin) GetAllTwins(db *gorm.DB) (*[]Twin, error) {
	var err error
	listOfTwins := []Twin{}

	err = db.Debug().Model(&Twin{}).Limit(100).Find(&listOfTwins).Error

	if err != nil {
		return &[]Twin{}, err
	}

	return &listOfTwins, err
}

func (twin *Twin) SaveTwin(db *gorm.DB) (*Twin, error) {
	var ToBeSaved error
	ToBeSaved = db.Debug().Model(&Twin{}).Create(&twin).Error

	if ToBeSaved != nil{
		return &Twin{}, ToBeSaved
	}

	return twin, nil
}

func (twin *Twin) UpdateTwin(db *gorm.DB) (*Twin, error) {
	// check; validate later
	if err := db.Debug().Model(&Twin{}).Where("ThingId=?", twin.ThingId).Updates(Twin{
		ThingId: twin.ThingId,
		NamedTwin: twin.NamedTwin,
		LastModified: time.Now(),
		Value: twin.Value}).Error;

	err != nil {
		return &Twin{}, err
	}

	//db.Debug().Save(&twin)

	return twin, nil
}

func (twin *Twin) DeleteTwin(db *gorm.DB) error {
	if err := db.Debug().Model(&Twin{}).Where("ThingId=?", twin.ThingId).Delete(&Twin{}, twin.ThingId).Error;

	err != nil{
		return err
	}

	return nil
}
