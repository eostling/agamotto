package Model

import (
	"os"
	"sync"
)

type Options struct{
	SegmentSize int32
	SegmentCache int64
	isReadOnly bool
}

type bEntry struct {
	index uint64
	size  int
}

type Batch struct {
	entries []bEntry
	data   []byte
}

// RWMutex is a reader/writer mutual exclusion lock

// If a goroutine holds a RWMutex for reading and another goroutine might
// call Lock, no goroutine should expect to be able to acquire a read lock
// until the initial read lock is released. In particular, this prohibits
// recursive read locking. This is to ensure that the lock eventually becomes
// available; a blocked Lock call excludes new readers from acquiring the
// lock.

type Log struct {
	mutex      sync.RWMutex
	commonPath string
	options    Options
	open       bool
	closed     bool
	first uint64
	second uint64
	reusableBatch Batch
	segmentFile *os.File
	writeBatch Batch
	// LRU at this time is not entirely implemented
	segmentCache LRU
}



func Open(filePath string, options *Options) (*Log, error) {
	if options == nil {
		options = new (Options) // should set default options
	}

	// check segment size
	// check whether or not the segment cache is empty
	// read in the filePath
	// handle the error case
}




