package seed

import (
	"github.com/jinzhu/gorm"
	"log"
	"novetta/Agamotto/Api/V1/Model"
	"time"
)

var twins = []Model.Twin{
	Model.Twin{
		ThingId: 1,
		NamedTwin: "Named Twin 1",
		LastModified: time.Now(),
		Value: "Value 1",
	},
	Model.Twin{
		ThingId: 2,
		NamedTwin: "Named Twin 2",
		LastModified: time.Now(),
		Value: "Value 2",
	},
}

func Load(db *gorm.DB) {

	err := db.Debug().DropTableIfExists(&Model.Twin{}).Error

	if err != nil {
		log.Fatalf("cannot drop table: %v", err)
	}

	err = db.Debug().AutoMigrate(&Model.Twin{}).Error

	if err != nil {
		log.Fatalf("cannot migrate table: %v", err)
	}

	for i, _ := range twins {
		err = db.Debug().Model(&Model.Twin{}).Create(&twins[i]).Error

		if err != nil {
			log.Fatalf("cannot seed twins table: %v", err)
		}
	}
}
